use std::collections::HashMap;
use std::{println};
use std::fs;

fn main() {
    let contents = fs::read_to_string("input.txt")
        .expect("Should have been able to read the file");

    let mut elves = HashMap::new();
    let mut index: i32 = 0;
    let mut result: i32 = 0;

    for line in contents.lines() {
        if !!!line.is_empty() {
            let number: i32 = line
                .trim()
                .parse()
                .expect("Wanted a number");

            result += number;
        } else if line.is_empty() {
            elves.insert(index, result);
            index += 1;
            result = 0;
        }        
    }

    let mut final_value: i32 = 0;
    for (_, calories) in elves.iter() {
        if *calories > final_value {
            final_value = *calories;
        }
    }

    // Find top 3 elves.
    // Sort hashmap values.
    
    // Create a vecteur to sort values.
    let mut count_vec: Vec<_> = elves.iter().collect();
    count_vec.sort_by(|a, b| b.1.cmp(a.1));

    println!("res: {:#?}", count_vec.get(0));
    println!("res: {:#?}", count_vec.get(1));
    println!("res: {:#?}", count_vec.get(2));

    println!("final_value: {}", final_value);
}
