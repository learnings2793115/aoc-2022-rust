use std::{println};
use std::fs;

fn main() {

    #[derive(Debug)]
    struct Game(char, char);
    let mut strategy: Vec<Game> = Vec::new();
    let mut total_score: i32 = 0;

    let contents = fs::read_to_string("input.txt")
        .expect("Should have been able to read the file");

    for line in contents.lines() {
        let a = line.chars().nth(0).unwrap();
        let b = line.chars().nth(2).unwrap();
        let party = Game(a, b);
        strategy.push(party);
    }

    for party in strategy.iter(){
        let opponent = party.0;
        let myself: char = party.1;
        let mut score: i32 = 0;

        match opponent{
            'A' => {
                if myself == 'X' {
                    score = 0 + 3;
                };
                if myself == 'Y' {
                    score = 3 + 1;
                };
                if myself == 'Z' {
                    score = 6 + 2;
                };
            },
            'B' => {
                if myself == 'X' {
                    score = 0 + 1;
                };
                if myself == 'Y' {
                    score = 3 + 2;
                };
                if myself == 'Z' {
                    score = 6 + 3;
                };
            },
            'C' => {
                if myself == 'X' {
                    score = 0 + 2;
                };
                if myself == 'Y' {
                    score = 3 + 3;
                };
                if myself == 'Z' {
                    score = 6 + 1;
                };
            },
            _ => score = 0,
        }

        total_score += score;
    }
    println!("{}", total_score);
}
