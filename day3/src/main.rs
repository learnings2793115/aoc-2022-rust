#![feature(iter_array_chunks)]

use std::println;
use std::fs;
use std::collections::HashMap;


fn common_char(s1: String, s2: String) -> char{
    let mut g: char = '_';
    for c in s2.chars() {
        let _char_index_founded = s1.find(c);
        if _char_index_founded != None {
            g = s1.chars().nth(_char_index_founded.unwrap()).unwrap();
            break;
        }
    }
    return g;
}

fn main() {
    #[derive(Debug)]

    // FIRST PART

    struct Rucksack(String, String);
    let mut loading: Vec<Rucksack> = Vec::new();

    let contents = fs::read_to_string("input.txt")
        .expect("Should have been able to read the file");

    for line in contents.lines() {
        let mid: usize = line.len() / 2;
        let (one, two) = line.split_at(mid);
        let rucksack = Rucksack(String::from(one), String::from(two));
        loading.push(rucksack);
    }

    // Now I must find doublon
    let mut common_chars: Vec<char> = Vec::new();
    for item in loading.iter() {
        common_chars.push(common_char(item.0.to_string(), item.1.to_string()));
    }

    let mut result: usize = 0;
    for c in common_chars.iter(){
        let char_index_min_alpha = String::from("abcdefghijklmnopqrstuvwxyz").find(*c);
        let char_index_maj_alpha = String::from("ABCDEFGHIJKLMNOPQRSTUVWXYZ").find(*c);
        if char_index_min_alpha != None {
            let point: usize = char_index_min_alpha.unwrap() + 1;
            result += point;
        }

        if char_index_maj_alpha != None {
            let point: usize = char_index_maj_alpha.unwrap() + 27;
            result += point;
        }
    }
    println!("{:?}", result);

    // SECOND PART

    let file = fs::read_to_string("./input.txt").unwrap();
    println!("{}", process_part2(&file));
}


pub fn process_part2(input: &str) -> String {
    let letter_scores = ('a'..='z')
        .chain('A'..='Z')
        .enumerate()
        .map(|(idx, c)| (c, idx + 1))
        .collect::<HashMap<char, usize>>();

    let result = input
        .lines()
        .array_chunks::<3>()
        .map(|[a, b, c]| {
            let common_char = a
                .chars()
                .find(|a_char| {
                    b.contains(*a_char)
                        && c.contains(*a_char)
                })
                .unwrap();
            letter_scores.get(&common_char).unwrap()
        })
        .sum::<usize>();

    result.to_string()
}
